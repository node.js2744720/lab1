const fs = require('fs');
const path = require('path');
const filePath = path.join(__dirname, 'user.json');

const getUser = () => {
    try {
        const userData = fs.readFileSync(filePath);
        return JSON.parse(userData);
    } catch (error) {
        console.error("Помилка читання даних користувача:", error);
        return null;
    }
};

const saveUser = (userData) => {
    try {
        fs.writeFileSync(filePath, JSON.stringify(userData, null, 2));
        console.log("Дані користувача збережено.");
    } catch (error) {
        console.error("Помилка збереження даних користувача:", error);
    }
};

const addLanguage = (language) => {
    const user = getUser();
    if (user) {
        if (!user.languages.some(lang => lang.title === language.title)) {
            user.languages.push(language);
            saveUser(user);
            console.log(`Мова "${language.title}" додано до користувача.`);
        } else {
            console.log(`Користувач уже знає "${language.title}".`);
        }
    }
};

const viewLanguages = () => {
    const user = getUser();
    if (user) {
        console.log("Мови користувача:");
        user.languages.forEach((language, index) => {
            console.log(`${index + 1}. Назва: ${language.title}, Рівень: ${language.level}`);
        });
    }
};

const removeLanguage = (languageTitle) => {
    const user = getUser();
    if (user) {
        const languageIndex = user.languages.findIndex(lang => lang.title === languageTitle);
        if (languageIndex !== -1) {
            user.languages.splice(languageIndex, 1);
            saveUser(user);
            console.log(`Мова "${languageTitle}" видалено від користувача.`);
        } else {
            console.log(`Користувач не знає "${languageTitle}".`);
        }
    }
};

module.exports = { addLanguage, viewLanguages, removeLanguage };