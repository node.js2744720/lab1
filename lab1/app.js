const yargs = require('yargs');
const userModule = require('./user');

// Додавання команди для додавання мови
yargs.command({
    command: 'addLanguage',
    describe: 'Додати мову користувачу',
    builder: {
        title: {
            describe: 'Назва мови',
            demandOption: true,
            type: 'string'
        },
        level: {
            describe: 'Рівень володіння',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        const language = {
            title: argv.title,
            level: argv.level
        };
        userModule.addLanguage(language);
    }
});

// Додавання команди для перегляду мов користувача
yargs.command({
    command: 'viewLanguages',
    describe: 'Переглянути мови користувача',
    handler() {
        userModule.viewLanguages();
    }
});

// Додавання команди для видалення мови
yargs.command({
    command: 'removeLanguage',
    describe: 'Видалити мову користувача',
    builder: {
        title: {
            describe: 'Назва мови для видалення',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        userModule.removeLanguage(argv.title);
    }
});

// Парсинг команд
yargs.parse();