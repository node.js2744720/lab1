const _ = require('lodash');
const fs = require('fs');

const numbers = [1, 2, 3, 4, 5];

// Метод 1: _.chunk()
const chunkedArray = _.chunk(numbers, 2);
console.log('Розбитий масив:', chunkedArray);

// Метод 2: _.map()
const mappedArray = _.map(numbers, num => num * 2);
console.log('Відображений масив:', mappedArray);

// Метод 3: _.filter()
const filteredArray = _.filter(numbers, num => num % 2 === 0);
console.log('Відфільтрований масив:', filteredArray);

// Метод 4: _.reduce()
const reducedValue = _.reduce(numbers, (sum, num) => sum + num, 0);
console.log('Сума масиву:', reducedValue);

// Метод 5: _.sortBy()
const sortedArray = _.sortBy(numbers, num => -num);
console.log('Відсортований масив:', sortedArray);

const results = {
    chunkedArray,
    mappedArray,
    filteredArray,
    reducedValue,
    sortedArray
};

fs.writeFile('./process.argv', JSON.stringify(results), err => {
   if (err) throw err;
   console.log('Результати були записані в файл process.argv');
});