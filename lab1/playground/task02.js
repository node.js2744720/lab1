const fs = require('fs');

const newLine = "Hello, World!";

const filePath = './task02.txt';

function addLineToFile(line) {
    fs.appendFile(filePath, line + '\n', (err) => {
        if (err) {
            console.error('Помилка запису у файл:', err);
            return;
        }
        console.log('Рядок успішно додано до файлу.');
    });
}

addLineToFile(newLine);